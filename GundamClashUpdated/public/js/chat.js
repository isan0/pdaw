$(document).ready(function () {
  
    //Inicializa socket con IO
    const socket = io();
    var link = window.location.href;
    var currentRoom = link.substr(link.lastIndexOf('/') + 1);
    
    //Accion cuando el usuario envia mensaje con submit
    $("#chat").submit(function (e) {
      e.preventDefault();
      var msg = $("#msg").val();
      var userAuth = $("#autor").val();
      if (currentRoom == "partida"){
      $("#chatBox").append(
        `<div class="containerChat blueTeam">
        <img class="right" src="imgs/placeholderpfp.png" alt="Avatar" style="width:100%;">
          <p  class="blackened">${msg}</p>
          <span class="time-right">You</span>
    </div>`
      );

      } else {
      $("#chatBox").append(
        `<div class="containerChat darker">
        <img class="right" src="imgs/placeholderpfp.png" alt="Avatar" style="width:100%;">
          <p>${msg}</p>
          <span class="time-right">You</span>
    </div>`
      );
    }

      var toSend = { room: currentRoom, text: msg, autor: userAuth }
      socket.emit("newMSG", toSend)
    });
  
    //Acciones a realizar cuando se detecta actividad en el canal newMsg
      socket.on("newMSG", (data) => {
        var head = ""
        var messageRoom = data["room"];
        if (currentRoom == "partida" && messageRoom == "partida"){
          $("#chatBox").append(
            `<div class="containerChat redTeam" style="text-align:end;">
            <img src="imgs/placeholderpfp.png" alt="Avatar" style="width:100%;">
              <p  class="blackened">${data["text"]}</p>
              <span class="time-left">${head}${data["user"]}</span>
            </div>`
              );
        } else if (currentRoom != "partida" && messageRoom != "partida"){
          $("#chatBox").append(
            `<div class="containerChat" style="text-align:end;">
            <img src="imgs/placeholderpfp.png" alt="Avatar" style="width:100%;">
              <p>${data["text"]}</p>
              <span class="time-left">${head}${data["user"]}</span>
            </div>`
              );
        }
    })
  });