var express = require("express");
router = express.Router();
var TeambuilderCntr = require(__dirname + "/../controllers/modelos");
var TeamCntr = require(__dirname + "/../controllers/home");
var PerfilCntr = require(__dirname + "/../controllers/perfil");


router.route('/').get(async (req, res, next) => {
    var resultModels = await TeambuilderCntr.load();
    var resultTeam = await TeamCntr.load();
    var resultPerf = await PerfilCntr.load();
    var usuarioGuardado = req.session.usuario;
    var userStuff = await PerfilCntr.loadType(usuarioGuardado);
    if(usuarioGuardado == ""){
        res.redirect("/login");
    } else {
        res.render("teambuilder", { Gundams: resultModels, Team: resultTeam, perfilActual: resultPerf, trueUser: userStuff});
    }
});

router.route('/add/:id').get( async (req, res, next) => {
    var resultTeam = await TeamCntr.load();

    var haroForm = {
        gundamModel: 'Gundam Haro',
        lifePoints: '70',
        shootPower: '20',
        shootRange: '3-5',
        movement: '9',
        movementType: 'Aereo',
        capacity: '1',
        image: 'imgs/gundamharostock.jpg',
        descrip: 'Un Gundam pequeño con alta movilidad.',
        imageMini: 'imgs/gundamHaro.png'
    };
    var cowForm = {
        gundamModel: 'Gundam Cow',
        lifePoints: '100',
        shootPower: '30',
        shootRange: '2-4',
        movement: '4',
        movementType: 'Terrestre',
        capacity: '2',
        image: 'imgs/gundamcowstock.jpg',
        descrip: 'Un Gundam que es mooo rapido y potente.',
        imageMini: 'imgs/gundamcowstock.jpg'
    };
    var assaultForm = {
        gundamModel: 'Gundam Assault',
        lifePoints: '90',
        shootPower: '30',
        shootRange: '3-5',
        movement: '6',
        movementType: 'Terrestre',
        capacity: '1',
        image: 'imgs/gundamassaultstock.jpg',
        descrip: 'Un Gundam estandar y bastante balanceado.',
        imageMini: 'imgs/gundamassaultstock.jpg'
    };
    var shieldForm = {
        gundamModel: 'Gundam Shield',
        lifePoints: '120',
        shootPower: '40',
        shootRange: '1-3',
        movement: '4',
        movementType: 'Terrestre',
        capacity: '1',
        image: 'imgs/gundamshieldstock.jpg',
        descrip: 'Un Gundam con mucha defensa y poca movilidad.',
        imageMini: 'imgs/gundamshieldstock.jpg'
    };

    if(resultTeam.length < 5){
        switch (req.params.id) {
            case 'Gundam Haro':
                TeamCntr.save(haroForm);
                break;
            case 'Gundam Cow':
                TeamCntr.save(cowForm);
                break;
            case 'Gundam Assault':
                TeamCntr.save(assaultForm);
                break;
            case 'Gundam Shield':
                TeamCntr.save(shieldForm);
                break;
        }
    } else {
        console.log("Numero máximo de Gundam alcanzado");
    }
    res.redirect("/teambuilder");

})

router.route('/delete/:id').get(async (req, res, next) => {
    TeamCntr.product_delete(req.params.id);
    res.redirect("/modelos");
});
module.exports = router;