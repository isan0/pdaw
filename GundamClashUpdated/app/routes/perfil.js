var express = require("express");
router = express.Router();
var PerfilCntr = require(__dirname + "/../controllers/perfil");
var HomeCntr = require(__dirname + "/../controllers/home");

router.route('/').get(async (req, res, next) => {
    var result = await PerfilCntr.load();
    var currentTeam = await HomeCntr.load();
    var usuarioGuardado = req.session.usuario;
    var userStuff = await PerfilCntr.loadType(usuarioGuardado);
    if(usuarioGuardado == ""){
        res.redirect("/login");
    } else {
        res.render("perfil", { perfilActual: result, CurrentTeam: currentTeam, trueUser: userStuff});
    }
});

router.route('/:username').get(async (req, res, next) => {
    var perfilType = await PerfilCntr.loadType(req.params.username);
    var result = await PerfilCntr.load();
    var currentTeam = await HomeCntr.load();
    var userStuff = await PerfilCntr.loadType(req.session.usuario);

    res.render("perfil", { perfilActual: result, perfilUsuario: perfilType, CurrentTeam: currentTeam, trueUser: userStuff});
});

router.post('/newPerf', async (req,res) => {
    var perfilTest = {
        username: req.body.username,
        winrate: '7W-8L',
        email: req.body.email,
        password: req.body.password,
        combattotal: '10',
        favoritegundam: 'Gundam Haro',
        icon: 'imgs/placeholderpfp.png',
        status: '(Sin estado)',
    };
    PerfilCntr.save(perfilTest);
    res.redirect("/perfil");
});


module.exports = router;