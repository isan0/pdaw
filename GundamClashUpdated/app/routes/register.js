var express = require("express");
router = express.Router();
var RegisterCntr = require(__dirname + "/../controllers/register");
var PerfilCntr = require(__dirname + "/../controllers/perfil");


router.route('/').get(async (req, res, next) => {
    var result = await RegisterCntr.load();
    req.session.profErrors = [];
    var registerErrors = req.session.regErrors || [];

    res.render("register", {res: result, errors: registerErrors});
});

router.post('/newPerf', async (req,res) => {

    var userStuff = await PerfilCntr.checkNameUse(req.body.username);
    var userEmail = await PerfilCntr.checkEmailUse(req.body.email);
    req.session.regErrors = [];
    var registerErrors = req.session.regErrors || [];
    var validRegister = true;

    if (userStuff != null) {
        console.log("Nombre de usuario en uso");
        registerErrors.push("Nombre de usuario en uso");
        validRegister = false;
    }
    if (userEmail != null) {
        console.log("Email en uso");
        registerErrors.push("Email en uso");
        validRegister = false;
    }
    if (req.body.password != req.body.c_password) {
        console.log("Las contraseñas no coinciden");
        registerErrors.push("Las contraseñas no coinciden");
        validRegister = false;
    }

    if (validRegister){
        var perfilTest = {
            username: req.body.username,
            winrate: '0W-0L',
            email: req.body.email,
            password: req.body.password,
            combattotal: '0',
            favoritegundam: 'Undecided',
            icon: 'imgs/placeholderpfp.png',
        };
        req.session.usuario = req.body.username;
        PerfilCntr.save(perfilTest);
        res.redirect("/home");
    } else {
        res.redirect("/register");
    }

});

module.exports = router;