var express = require("express");
router = express.Router();
var PartidaCntr = require(__dirname + "/../controllers/partida");
var TeamCntr = require(__dirname + "/../controllers/home");
var PerfilCntr = require(__dirname + "/../controllers/perfil");
var HomeCntr = require(__dirname + "/../controllers/home");



router.route('/').get(async (req, res, next) => {
    var result = await TeamCntr.load();
    var resultPerf = await PerfilCntr.load();
    var usuarioGuardado = req.session.usuario;
    var userStuff = await PerfilCntr.loadType(usuarioGuardado);
    var currentTeam = await HomeCntr.load();
    if(usuarioGuardado == ""){
        res.redirect("/login");
    } else {
        res.render("partida", { Team: result, perfilActual: resultPerf, trueUser: userStuff, CurrentTeam: currentTeam});
    }
});

module.exports = router;