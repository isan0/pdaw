var express = require("express");
router = express.Router();
var ModelosCntr = require(__dirname + "/../controllers/modelos");
var PerfilCntr = require(__dirname + "/../controllers/perfil");


router.route('/').get(async (req, res, next) => {
    var result = await ModelosCntr.load();
    var resultPerf = await PerfilCntr.load();
    var usuarioGuardado = req.session.usuario;
    var userStuff = await PerfilCntr.loadType(usuarioGuardado);
    if(usuarioGuardado == ""){
        res.redirect("/login");
    } else {
        res.render("modelos", { Gundams: result, perfilActual: resultPerf, trueUser: userStuff});
    }
});


router.route('/new').get((req, res, next) => {
    var haroForm = {
        gundamModel: 'Gundam Haro',
        lifePoints: '70',
        shootPower: '20',
        shootRange: '3-5',
        movement: '9',
        movementType: 'Aereo',
        capacity: '1',
        image: 'imgs/gundamharostock.jpg',
        descrip: 'Un Gundam pequeño con alta movilidad.',
        imageMini: 'imgs/gundamHaro.png'
    };
    var cowForm = {
        gundamModel: 'Gundam Cow',
        lifePoints: '100',
        shootPower: '30',
        shootRange: '2-4',
        movement: '4',
        movementType: 'Terrestre',
        capacity: '2',
        image: 'imgs/gundamcowstock.jpg',
        descrip: 'Un Gundam que es mooo rapido y potente.',
        imageMini: 'imgs/gundamcowstock.png'
    };
    var assaultForm = {
        gundamModel: 'Gundam Assault',
        lifePoints: '90',
        shootPower: '30',
        shootRange: '3-5',
        movement: '6',
        movementType: 'Terrestre',
        capacity: '1',
        image: 'imgs/gundamassaultstock.jpg',
        descrip: 'Un Gundam estandar y bastante balanceado.',
        imageMini: 'imgs/gundamassaultstock.png'
    };
    var shieldForm = {
        gundamModel: 'Gundam Shield',
        lifePoints: '120',
        shootPower: '40',
        shootRange: '1-3',
        movement: '4',
        movementType: 'Terrestre',
        capacity: '1',
        image: 'imgs/gundamshieldstock.jpg',
        descrip: 'Un Gundam con mucha defensa y poca movilidad.',
        imageMini: 'imgs/gundamshieldstock.png'
    };
    ModelosCntr.save(haroForm);
    ModelosCntr.save(cowForm);
    ModelosCntr.save(assaultForm);
    ModelosCntr.save(shieldForm);

    res.redirect("/login");
});


router.route('/delete/:id').get(async (req, res, next) => {
    ModelosCntr.product_delete(req.params.id);
    res.redirect("/modelos");
});

module.exports = router;