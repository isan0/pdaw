var express = require("express");
router = express.Router();
var HomeCntr = require(__dirname + "/../controllers/home");
var PerfilCntr = require(__dirname + "/../controllers/perfil");


router.route('/').get(async (req, res, next) => {
    var result = await HomeCntr.load();
    var resultPerf = await PerfilCntr.load();
    var usuarioGuardado = req.session.usuario;
    var userStuff = await PerfilCntr.loadType(usuarioGuardado);
    if(usuarioGuardado == ""){
        res.redirect("/login");
    } else {
        console.log("Logged as " + usuarioGuardado);
        res.render("home", { CurrentTeam: result, perfilActual: resultPerf, trueUser: userStuff });
    }
});

router.route('/delete/:id').get(async (req, res, next) => {
    HomeCntr.product_delete(req.params.id);
    res.redirect("/teambuilder");
});

module.exports = router;