var express = require("express");
router = express.Router();
var PerfilupdateCntr = require(__dirname + "/../controllers/perfilupdate");
var PerfilCntr = require(__dirname + "/../controllers/perfil");


router.route('/').get(async (req, res, next) => {
    var result = await PerfilCntr.load();
    var usuarioGuardado = req.session.usuario;
    var userStuff = await PerfilCntr.loadType(usuarioGuardado);
    if(usuarioGuardado == ""){
        res.redirect("/login");
    } else {
        res.render("perfilupdate", { perfilActual: result, trueUser: userStuff});
    }
});

router.route('/:username').get(async (req, res, next) => {
    var result = await PerfilCntr.loadType(req.params.username);
    res.render("perfilupdate", { perfilActual: result });
});

router.route('/new').get((req, res, next) => {
    var perfilupdateTest = {
        username: 'GundamuPlayer23',
        winrate: '7W-8L',
        combattotal: '10',
        favoritegundam: 'Gundam Haro',
        icon: 'imgs/placeholderpfp.png',
    };
    PerfilupdateCntr.save(perfilupdateTest);

    res.redirect("/perfilupdate");
});

router.post('/update/:id', async (req, res) => {
    PerfilupdateCntr.perfil_update(req.params.id, req.body.username, req.body.password, req.body.favoritegundam, req.body.status, req.body.icon);
    req.session.usuario = req.body.username;
    res.redirect("/perfil/" + req.body.username);
});

router.post('/delete/:id', async (req, res) => {
    PerfilupdateCntr.perfil_delete(req.params.id);
    res.redirect("/login");
});

module.exports = router;