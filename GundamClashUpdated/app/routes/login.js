var express = require("express");
router = express.Router();
var PerfilCntr = require(__dirname + "/../controllers/perfil");


router.route('/').get(async (req, res, next) => {
    req.session.usuario = "";
    req.session.regErrors = [];
    var profileErrors = req.session.profErrors || [];
    res.render("login", {errors: profileErrors});
});

router.post('/perfilCheck', async (req,res) => {
    req.session.profErrors = [];
    var profileErrors = req.session.profErrors || [];
    var loginCheck = await PerfilCntr.checkPass(req.body.username,req.body.password);
    if (loginCheck != null){
        req.session.usuario = req.body.username;
        res.redirect("/home");
    } else {
        console.log("Usuario o contraseña incorrecta");
        profileErrors.push("Usuario o contraseña incorrecta");
        res.redirect("/login");
    }
});

module.exports = router;