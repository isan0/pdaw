var express = require("express");
router = express.Router();

var HomeCntr = require(__dirname + "/../controllers/home");
var TeambuilderCntr = require(__dirname + "/../controllers/modelos");
var PerfilCntr = require(__dirname + "/../controllers/perfil");
var PerfilupdateCntr = require(__dirname + "/../controllers/perfilupdate");
var RegisterCntr = require(__dirname + "/../controllers/register");


router.route('/').get(async (req, res, next) => {
    res.redirect("/login");
});

/* Home.Js */

router.route('/home').get(async (req, res, next) => {
    var result = await HomeCntr.load();
    var resultPerf = await PerfilCntr.load();
    var userStuff = await PerfilCntr.loadType(req.session.usuario);
    var usuarioTest = req.session.usuario;
    console.log(result);
    console.log(result.length);
    console.log(usuarioTest);
    res.render("home", { CurrentTeam: result, perfilActual: resultPerf, trueUser: userStuff });
});

router.route('/home/delete/:id').get(async (req, res, next) => {
    HomeCntr.product_delete(req.params.id);
    res.redirect("/teambuilder");
});

/* Login.Js */

router.route('/login').get(async (req, res, next) => {
    res.render("login");
});

router.post('/login/perfilCheck', async (req,res) => {
    var userStuff = await PerfilCntr.loadType(req.body.username);
    var userPass = await PerfilCntr.loadPass(req.body.password);
    req.session.usuario = req.body.username;
    console.log(userStuff);
    console.log(userPass);
    res.redirect("/home");
});

/* Modelos.Js (Gundam) */

router.route('/modelos').get(async (req, res, next) => {
    var result = await ModelosCntr.load();
    var resultPerf = await PerfilCntr.load();
    var userStuff = await PerfilCntr.loadType(req.session.usuario);

    console.log(result);
    if (result.length < 1) {
        res.redirect("/modelos/new");
    }
    res.render("modelos", { Gundams: result, perfilActual: resultPerf, trueUser: userStuff});
});


router.route('/modelos/new').get((req, res, next) => {
    var haroForm = {
        gundamModel: 'Gundam Haro',
        lifePoints: '70',
        shootPower: '20',
        shootRange: '3-5',
        movement: '9',
        movementType: 'Aereo',
        capacity: '1',
        image: 'imgs/gundamharostock.jpg',
        descrip: 'Un Gundam pequeño con alta movilidad.',
        imageMini: 'imgs/gundamHaro.png'
    };
    var cowForm = {
        gundamModel: 'Gundam Cow',
        lifePoints: '100',
        shootPower: '30',
        shootRange: '2-4',
        movement: '4',
        movementType: 'Terrestre',
        capacity: '2',
        image: 'imgs/gundamcowstock.jpg',
        descrip: 'Un Gundam que es mooo rapido y potente.',
        imageMini: 'imgs/gundamcowstock.png'
    };
    var assaultForm = {
        gundamModel: 'Gundam Assault',
        lifePoints: '90',
        shootPower: '30',
        shootRange: '3-5',
        movement: '6',
        movementType: 'Terrestre',
        capacity: '1',
        image: 'imgs/gundamassaultstock.jpg',
        descrip: 'Un Gundam estandar y bastante balanceado.',
        imageMini: 'imgs/gundamassaultstock.png'
    };
    var shieldForm = {
        gundamModel: 'Gundam Shield',
        lifePoints: '120',
        shootPower: '40',
        shootRange: '1-3',
        movement: '4',
        movementType: 'Terrestre',
        capacity: '1',
        image: 'imgs/gundamshieldstock.jpg',
        descrip: 'Un Gundam con mucha defensa y poca movilidad.',
        imageMini: 'imgs/gundamshieldstock.png'
    };
    ModelosCntr.save(haroForm);
    ModelosCntr.save(cowForm);
    ModelosCntr.save(assaultForm);
    ModelosCntr.save(shieldForm);

    res.redirect("/modelos");
});

router.route('/modelos/delete/:id').get(async (req, res, next) => {
    ModelosCntr.product_delete(req.params.id);
    res.redirect("/modelos");
});

/* Partida.Js */

router.route('/partida/').get(async (req, res, next) => {
    var result = await HomeCntr.load();
    var resultPerf = await PerfilCntr.load();
    var userStuff = await PerfilCntr.loadType(req.session.usuario);

    console.log(result);
    res.render("partida", { Team: result, perfilActual: resultPerf, trueUser: userStuff});
});

/* Perfil.js */

router.route('/perfil').get(async (req, res, next) => {
    var result = await PerfilCntr.load();
    var currentTeam = await HomeCntr.load();
    var userStuff = await PerfilCntr.loadType(req.session.usuario);

    console.log(result);
    if (result.length < 1) {
        res.redirect("/perfil/new");
    }
    res.render("perfil", { perfilActual: result, CurrentTeam: currentTeam, trueUser: userStuff});
});

router.route('/perfil/:username').get(async (req, res, next) => {
    var perfilType = await PerfilCntr.loadType(req.params.username);
    var result = await PerfilCntr.load();
    var currentTeam = await HomeCntr.load();
    var userStuff = await PerfilCntr.loadType(req.session.usuario);

    console.log(result);
    res.render("perfil", { perfilActual: result, perfilUsuario: perfilType, CurrentTeam: currentTeam, trueUser: userStuff});
});


router.route('/perfil/new').get((req, res, next) => {
    var perfilTest = {
        username: 'GundamuPlayer23',
        winrate: '7W-8L',
        email: 'gundam@gmail.com',
        password: 'gundam12',
        combattotal: '10',
        favoritegundam: 'Gundam Haro',
        icon: 'imgs/placeholderpfp.png',
    };
    PerfilCntr.save(perfilTest);

    res.redirect("/perfil");
});

router.post('/perfil/newPerf', async (req,res) => {
    var perfilTest = {
        username: req.body.username,
        winrate: '7W-8L',
        email: req.body.email,
        password: req.body.password,
        combattotal: '10',
        favoritegundam: 'Gundam Haro',
        icon: 'imgs/placeholderpfp.png',
        status: '(Sin estado)',
    };
    PerfilCntr.save(perfilTest);
    res.redirect("/perfil");
});

/*Perfilupdate.Js*/

router.route('/perfilupdate').get(async (req, res, next) => {
    var result = await PerfilCntr.load();
    var userStuff = await PerfilCntr.loadType(req.session.usuario);

    console.log(result);
    res.render("perfilupdate", { perfilActual: result, trueUser: userStuff});
});

router.route('/perfilupdate/:username').get(async (req, res, next) => {
    var result = await PerfilCntr.loadType(req.params.username);
    console.log(result);
    res.render("perfilupdate", { perfilActual: result });
});

router.route('/perfilupdate/new').get((req, res, next) => {
    var perfilupdateTest = {
        username: 'GundamuPlayer23',
        winrate: '7W-8L',
        combattotal: '10',
        favoritegundam: 'Gundam Haro',
        icon: 'imgs/placeholderpfp.png',
    };
    PerfilupdateCntr.save(perfilupdateTest);

    res.redirect("/perfilupdate");
});

router.post('/perfilupdate/update/:id', async (req, res) => {
    PerfilupdateCntr.perfil_update(req.params.id, req.body.username, req.body.password, req.body.favoritegundam, req.body.status, req.body.icon);
    req.session.usuario = req.body.username;
    res.redirect("/perfil/" + req.body.username);
});

router.post('/perfilupdate/delete/:id', async (req, res) => {
    PerfilupdateCntr.perfil_delete(req.params.id);
    res.redirect("/login");
});

/* Register.Js */

router.route('/register').get(async (req, res, next) => {
    var result = await RegisterCntr.load();
    console.log(result);
    res.render("register", result);
});

router.post('/register/newPerf', async (req,res) => {
    var perfilTest = {
        username: req.body.username,
        winrate: '0W-0L',
        email: req.body.email,
        password: req.body.password,
        combattotal: '0',
        favoritegundam: 'Undecided',
        icon: 'imgs/placeholderpfp.png',
    };
    req.session.usuario = req.body.username;
    PerfilCntr.save(perfilTest);
    res.redirect("/home");
});

/* Teambuilder.Js */

router.route('/teambuilder').get(async (req, res, next) => {
    var resultModels = await TeambuilderCntr.load();
    var resultTeam = await HomeCntr.load();
    var resultPerf = await PerfilCntr.load();
    var userStuff = await PerfilCntr.loadType(req.session.usuario);

    console.log(resultModels);
    res.render("teambuilder", { Gundams: resultModels, Team: resultTeam, perfilActual: resultPerf, trueUser: userStuff});
});

router.route('/teambuilder/add/:id').get((req, res, next) => {
    var haroForm = {
        gundamModel: 'Gundam Haro',
        lifePoints: '70',
        shootPower: '20',
        shootRange: '3-5',
        movement: '9',
        movementType: 'Aereo',
        capacity: '1',
        image: 'imgs/gundamharostock.jpg',
        descrip: 'Un Gundam pequeño con alta movilidad.',
        imageMini: 'imgs/gundamHaro.png'
    };
    var cowForm = {
        gundamModel: 'Gundam Cow',
        lifePoints: '100',
        shootPower: '30',
        shootRange: '2-4',
        movement: '4',
        movementType: 'Terrestre',
        capacity: '2',
        image: 'imgs/gundamcowstock.jpg',
        descrip: 'Un Gundam que es mooo rapido y potente.',
        imageMini: 'imgs/gundamcowstock.jpg'
    };
    var assaultForm = {
        gundamModel: 'Gundam Assault',
        lifePoints: '90',
        shootPower: '30',
        shootRange: '3-5',
        movement: '6',
        movementType: 'Terrestre',
        capacity: '1',
        image: 'imgs/gundamassaultstock.jpg',
        descrip: 'Un Gundam estandar y bastante balanceado.',
        imageMini: 'imgs/gundamassaultstock.jpg'
    };
    var shieldForm = {
        gundamModel: 'Gundam Shield',
        lifePoints: '120',
        shootPower: '40',
        shootRange: '1-3',
        movement: '4',
        movementType: 'Terrestre',
        capacity: '1',
        image: 'imgs/gundamshieldstock.jpg',
        descrip: 'Un Gundam con mucha defensa y poca movilidad.',
        imageMini: 'imgs/gundamshieldstock.jpg'
    };
    switch (req.params.id) {
        case 'Gundam Haro':
            HomeCntr.save(haroForm);
            break;
        case 'Gundam Cow':
            HomeCntr.save(cowForm);
            break;
        case 'Gundam Assault':
            HomeCntr.save(assaultForm);
            break;
        case 'Gundam Shield':
            HomeCntr.save(shieldForm);
            break;
    }
    res.redirect("/teambuilder");

})

router.route('/teambuilder/delete/:id').get(async (req, res, next) => {
    HomeCntr.product_delete(req.params.id);
    res.redirect("/modelos");
});

module.exports = router;
