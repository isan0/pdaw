var mongoose = require("mongoose"),
    Schema = mongoose.Schema

var perfilupdateSchema = new Schema({
    username: { type: String },
    winrate: { type: String },
    combattotal: { type: String },
    favoritegundam: { type: String },
    icon: { type: String },
});

module.exports = mongoose.model("Perfilupdate", perfilupdateSchema);