var mongoose = require("mongoose"),
    Schema = mongoose.Schema

var homeSchema = new Schema({
    gundamModel: { type: String },
    lifePoints: { type: String },
    shootPower: { type: String },
    shootRange: { type: String },
    movement: { type: String },
    movementType: { type: String },
    capacity: { type: String },
    image: { type: String },
    descrip: { type: String },
    imageMini: { type: String },
});

module.exports = mongoose.model("Home", homeSchema);