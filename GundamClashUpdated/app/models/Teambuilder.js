var mongoose = require("mongoose"),
    Schema = mongoose.Schema

var teambuilderSchema = new Schema({
    placeholder: { type: String }
});

module.exports = mongoose.model("Teambuilder", teambuilderSchema);