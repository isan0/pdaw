var mongoose = require("mongoose"),
    Schema = mongoose.Schema

    var registerSchema = new Schema({
        username: { type: String },
        winrate: { type: String },
        email: { type: String },
        password: { type: String },
        combattotal: { type: String },
        favoritegundam: { type: String },
        icon: { type: String },
    });

module.exports = mongoose.model("Register", registerSchema);