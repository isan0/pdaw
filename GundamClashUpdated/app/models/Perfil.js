var mongoose = require("mongoose"),
    Schema = mongoose.Schema

var perfilSchema = new Schema({
    username: { type: String },
    winrate: { type: String },
    email: { type: String },
    password: { type: String },
    combattotal: { type: String },
    favoritegundam: { type: String },
    icon: { type: String },
    status: { type: String },
});

module.exports = mongoose.model("Perfil", perfilSchema);