var moongose = require("mongoose"),
    Perfil = require("../models/Perfil");

exports.load = async () => {
    var res = await Perfilupdate.find({})
    return res;
}

exports.loadType=async(username)=>{
    var res= await Perfil.find({"username": username})
    return res;
}

exports.save = (req) => {
    var newPerfilupdate = new Perfilupdate(req);
    newPerfilupdate.save((err, res) => {
        if (err) console.log(err);
        console.log("Insertado en la BD");
        return res;
    });
}

exports.perfil_update = function (id, newUsername, newPassword, newGundamFav, newStatus, newIcon) {
    var perfilToUpdate = {
        _id: id,
    };
    var newValues = {
        $set: {
            username: newUsername,
            password: newPassword,
            favoritegundam: newGundamFav,
            status: newStatus,
            icon: newIcon,
        }
    }
    Perfil.updateOne(perfilToUpdate, newValues, function (err) {
        if (err) return handleError(err);
    });
};

exports.perfil_delete = function (id) {
    var incDel = {
        _id: id,
    };
    Perfil.deleteOne(incDel, function (err) {
        if (err) return handleError(err);
    });
};

