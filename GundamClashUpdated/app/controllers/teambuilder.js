import Modelos from "../models/Modelos";
var moongose = require("mongoose"),
    Teambuilder = require("../models/Teambuilder");

exports.load = async () => {
    var res = await Teambuilder.find({})
    return res;
}
exports.save = (req) => {
    var newTeambuilder = new Teambuilder(req);
    newTeambuilder.save((err, res) => {
        if (err) console.log(err);
        console.log("Insertado en la BD");
        return res;
    });
}

