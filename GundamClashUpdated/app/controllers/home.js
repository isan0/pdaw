var moongose = require("mongoose"),
    Home = require("../models/Home");
    Perfil = require("../models/Perfil");

exports.load = async () => {
    var res = await Home.find({})
    return res;
}

exports.save = (req) => {
    var newHome = new Home(req);
    newHome.save((err, res) => {
        if (err) console.log(err);
        console.log("Insertado en la BD");
        return res;
    });
}

exports.product_delete = function (id) {
    var gundamToDelete = {
        _id: id,
    };
    Home.deleteOne(gundamToDelete, function (err) {
        if (err) return handleError(err);
    });
};