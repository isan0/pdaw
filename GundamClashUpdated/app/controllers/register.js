var moongose = require("mongoose"),
    Register = require("../models/Register");

exports.load = async () => {
    var res = await Register.find({})
    return res;
}
exports.save = (req) => {
    var newRegister = new Register(req);
    newRegister.save((err, res) => {
        if (err) console.log(err);
        console.log("Insertado en la BD");
        return res;
    });
}