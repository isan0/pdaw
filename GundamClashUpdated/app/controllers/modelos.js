var moongose = require("mongoose"),
    Modelos = require("../models/Modelos");

exports.load = async () => {
    var res = await Modelos.find({})
    return res;
}
exports.save = (req) => {
    var newModelos = new Modelos(req);
    newModelos.save((err, res) => {
        if (err) console.log(err);
        console.log("Insertado en la BD");
        return res;
    });
}

exports.product_delete = function (id) {
    var gundamToDelete = {
        _id: id,
    };
    Modelos.deleteOne(gundamToDelete, function (err) {
        if (err) return handleError(err);
    });
};
