var moongose = require("mongoose"),
    Perfil = require("../models/Perfil");

exports.load = async () => {
    var res = await Perfil.find({})
    return res;
}

exports.loadType=async(username)=>{
    var res= await Perfil.find({"username": username}, function (err, result) {
        if (err) { 

         }
        if (!result) {
            console.log("Nothin");
        }
    })
    return res;
}

exports.loadPass=async(password)=>{
    var res= await Perfil.find({"password": password})
    return res;
}

exports.checkNameUse=async(username)=>{
    var res= await Perfil.findOne({"username": username})
    return res;
}

exports.checkEmailUse=async(email)=>{
    var res= await Perfil.findOne({"email": email})
    return res;
}

exports.checkPass=async(username,password)=>{
    var res = await Perfil.findOne({"username": username, "password": password}, function (err, result){
        if(err){
            console.log(err);
        };
    });
    return res;
}

exports.save = (req) => {
    var newPerfil = new Perfil(req);
    newPerfil.save((err, res) => {
        if (err) console.log(err);
        console.log("Insertado en la BD");
        return res;
    });
}