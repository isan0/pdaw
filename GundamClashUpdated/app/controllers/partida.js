var moongose = require("mongoose"),
    Partida = require("../models/Partida");

exports.load = async () => {
    var res = await Partida.find({})
    return res;
}
exports.save = (req) => {
    var newPartida = new Partida(req);
    newPartida.save((err, res) => {
        if (err) console.log(err);
        console.log("Insertado en la BD");
        return res;
    });
}