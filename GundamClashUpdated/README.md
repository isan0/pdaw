# Proyecto Gundam

Vaciar la carpeta de data

(Iniciar docker - Solo necesario en Fedora)

sudo mkdir /sys/fs/cgroup/systemd

sudo mount -t cgroup -o none,name=systemd cgroup /sys/fs/cgroup/systemd 

(Abrir terminal)

(Si se ha instalado la aplicación previamente)

docker stop $(docker ps -a -q)

docker rm $(docker ps -a -q)

docker system prune -a 

(Para instalar en el docker)

docker-compose up -d

(Abrir el node alpine)

npm install 

npm run start

(En caso de que la database de eror, borrar todo el contenido de la carpeta "Data" y hacer un nuevo docker compose)

http://localhost:1040/

(Entrar **una sola vez** a este link para seedear los modelos gundam al **primer uso** de la página en caso de que la página Modelos esté vacía)

http://localhost:1040/modelos/new

Hacer /new en Modelos