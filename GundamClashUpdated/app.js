require("dotenv").config();
//const { Socket } = require("dgram");
var express = require("express");
var app = express();
var server = require("http").createServer(app);
var mongoose = require("mongoose");
var io = require("socket.io")(server)
var numUsers = 0;
const session = require('express-session');


server.listen(process.env.SERVER_PORT, (err, res) => {
    if (err) console.log("Error en el servidor" + err);
    console.log("Servidor conectado");
})

mongoose.connect(
    `mongodb://${process.env.MONGO_ROOT_USER}:${process.env.MONGO_ROOT_PASSWORD}@${process.env.MONGO_URI}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}?authSource=admin`,
    { useCreateIndex: true, useUnifiedTopology: true, useNewUrlParser: true },
    (err, res) => {
        if (err) console.log(`ERROR: connecting to Database: ${err}`);
        else console.log(`Database Online: ${process.env.MONGO_DB}`);
    }
);
var bodyParser = require('body-parser');

app.use(session({
    secret: 'secret-key',
    resave: false,
    saveUninitialized: false,
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static(__dirname + "/public"));
app.use(express.static('imgs'))
app.use(express.static('images'))
app.set("views", __dirname + "/views");
app.set("view engine", "pug");


var apiRoutes = require(__dirname + "/app/routes/api");
var modelosRoutes = require(__dirname + "/app/routes/modelos");
var homeRoutes = require(__dirname + "/app/routes/home");
var teambuilderRoutes = require(__dirname + "/app/routes/teambuilder");
var partidaRoutes = require(__dirname + "/app/routes/partida");
var perfilRoutes = require(__dirname + "/app/routes/perfil");
var perfilUpdateRoutes = require(__dirname + "/app/routes/perfilupdate");
var loginRoutes = require(__dirname + "/app/routes/login");
var registerRoutes = require(__dirname + "/app/routes/register");


app.use("/", loginRoutes);

app.use("/modelos", modelosRoutes);

app.use("/home", homeRoutes);

app.use("/teambuilder", teambuilderRoutes);

app.use("/partida", partidaRoutes);

app.use("/perfil", perfilRoutes);

app.use("/perfilupdate", perfilUpdateRoutes);

app.use("/login", loginRoutes);

app.use("/register", registerRoutes);



io.on("connection", (socket) => {
    console.log("Socket conectado");
    socket.user = "Usuario " + numUsers++;
    socket.join(socket.user);
    socket.on("newMSG", (data) => {
        data["user"] = data["autor"];
        console.log(data);
        socket.broadcast.emit("newMSG", data);
    })
    socket.on("newUser", (data) => {
        data["user"] = data["autor"];
        socket.broadcast.emit("newUser", data);
    })
  })
  
  module.exports = app;